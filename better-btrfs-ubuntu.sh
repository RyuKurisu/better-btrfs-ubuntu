#!/bin/sh
#Reconfigure default Btrfs set up of Ubuntu to have ‘root‘ @ and ‘home‘ @home subvolumes, needed for automatic snapshots.  Also replaced lzo with lz4 for performance for a slight increase in disk space. 
#Based a tutorial found on https://work-work.work/blog/2018/12/01/ubuntu-1804-btrfs.html

#Not every system installs to /dev/sda
#Originally /dev/sda1 in the tutorial, replaced with $EFI
$EFI=mount -l | grep /target/boot/efi ???
#Originally /dev/sda2 in the tutorial, replaced with $ROOT
$ROOT=mount -l | grep /target ???

sudo -i
umount /target/boot/efi
cd /target/
btrfs subvolume create @
btrfs subvolume create @home
mv -t @ b* d* e* h* i* l* m* o* p* r* s* t* u* v*
umount /target
mount -o compress=lz4,ssd,noatime,nodiratime,space_cache,discard,subvol=@ $ROOT /target
mount -o compress=lz4,ssd,noatime,nodiratime,space_cache,discard,subvol=@home $ROOT /target/home
mount $EFI /target/boot/efi
mount --bind /proc /target/proc
mount --bind /dev /target/dev
mount --bind /sys /target/sys
#Tutorial mentions lsblk -l, screenshot shows lsblk -f?
$UUID=lsblk -f? | grep ???
$UUID-EFI=lsblk -f? | grep ???
chroot /target/

#This isn't going to work scripted!!! sed per line
nano /etc/fstab
UUID=$UUID / btrfs compress=lz4,degraded,noatime,nodiratime,space_cache,ssd,subvol=@ 0 0  
UUID=$UUID /home btrfs compress=lz4,degraded,noatime,nodiratime,space_cache,ssd,subvol=@home 0 0  
UUID=$UUID-EFI /boot/efi vfat umask=0077 0 1
UUID=$UUID /mnt/btrfs_ssd  btrfs compress=lz4,degraded,noatime,nodiratime,space_cache,ssd     0 0
#To this point it won't work
update-initramfs -u -k all  
grub-install --recheck /dev/sda  
update-grub
#Check if sed command is valid!!
sed -i "s|GRUB_CMDLINE_LINUX=subvol=${rootsubvol} ${GRUB_CMDLINE_LINUX}"|GRUB_CMDLINE_LINUX="rootflags=degraded,subvol=${rootsubvol} ${GRUB_CMDLINE_LINUX}"|" /etc/grub.d/10_linux
update-grub



#Enable fstrim for Btrfs Async Discard, found at https://mutschler.eu/linux/install-guides/ubuntu-post-install/
#sed -i "s|,discard||" /etc/fstab 
#cat /etc/fstab #should be no discard 
#sed -i "s|,discard||" /etc/crypttab 
#cat /etc/crypttab #should be no discard 
systemctl enable fstrim.timer
