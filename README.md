# Better Btrfs for Ubuntu

The project's aim is to easily set up Ubuntu 18.04.3 and newer(kernel 5.0 and up) with a more usable Btrfs subvolume set up.

It'll feature subvolumes for @ @home and @swap.  By default it'll use encryption, but if someone raises an issue then I'll look into making it optional.
The @swap will be the size of the RAM to enable hibernation.